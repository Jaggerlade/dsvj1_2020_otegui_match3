#ifndef PLAYER_H
#define PLAYER_H
#include "raylib.h"
namespace Match3
{
	namespace player
	{
		struct MOUSE
		{
			Vector2 mousePos;
			bool isPressed;
		};
		extern MOUSE playerMouse;
		void init();
		void update();
		void draw();
		void deInit();
	}

}
#endif // !PLAYER_H