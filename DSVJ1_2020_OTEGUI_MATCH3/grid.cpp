#include "grid.h"
#include "player.h"
namespace Match3
{
	namespace grid
	{
		GRID gridArr[tamX][tamY];
		GRID gridInstance;
		Vector2 startPos;
		Color colorArr[5] = { BLACK,BLUE,GREEN,PURPLE,RED };
		Texture2D gemTextures[5];
		void init() 
		{
			gridInstance.gemShapes.width = 90;
			gridInstance.gemShapes.height = 90;
			startPos = {100,50};
			for (int i = 0; i < tamX; i++)
			{
				for (int j = 0; j < tamY; j++)
				{
					gridArr[i][j].gemShapes.width = gridInstance.gemShapes.width;
					gridArr[i][j].gemShapes.height = gridInstance.gemShapes.height;
					gridArr[i][j].gemShapes.x = startPos.x + ((gridInstance.gemShapes.width + 10) * i);
					gridArr[i][j].gemShapes.y = startPos.y + ((gridInstance.gemShapes.height + 10) * j);

					gridArr[i][j].gemColor = (COLOR_TYPE)GetRandomValue(0, 4);
					gridArr[i][j].colorGem = colorArr[gridArr[i][j].gemColor];
				}
			}
			loadTexture();
		}

		void update() 
		{
			for (int i = 0; i < tamX; i++)
			{
				for (int j = 0; j < tamY; j++)
				{
					if (CheckCollisionPointRec(player::playerMouse.mousePos,gridArr[i][j].gemShapes) && player::playerMouse.isPressed)
					{
						gridArr[i][j].gemColor = empty;
					}
				}
			}
		}
		void draw() 
		{
			for (int i = 0; i < tamX; i++)
			{
				for (int j = 0; j < tamY; j++)
				{
					switch (gridArr[i][j].gemColor)
					{
					case black:
						DrawTexture(gemTextures[black], (int)gridArr[i][j].gemShapes.x, (int)gridArr[i][j].gemShapes.y, WHITE);
						break;
					case blue:
						DrawTexture(gemTextures[blue], (int)gridArr[i][j].gemShapes.x, (int)gridArr[i][j].gemShapes.y, WHITE);
						break;
					case green:
						DrawTexture(gemTextures[green], (int)gridArr[i][j].gemShapes.x, (int)gridArr[i][j].gemShapes.y, WHITE);
						break;
					case purple:
						DrawTexture(gemTextures[purple], (int)gridArr[i][j].gemShapes.x, (int)gridArr[i][j].gemShapes.y, WHITE);
						break;
					case red:
						DrawTexture(gemTextures[red], (int)gridArr[i][j].gemShapes.x, (int)gridArr[i][j].gemShapes.y, WHITE);
						break;
					case empty:
						DrawRectangleRec(gridArr[i][j].gemShapes,BLACK);
					default:
						break;
					}					
				}
			}
		}
		void deInit() 
		{
			unLoadTexture();
		}
		void loadTexture()
		{
			Image reSize;
			for (int i = 0; i < 5; i++)
			{
				reSize = LoadImage(FormatText("res/Assets/Gems/Gem%i.png",i));
				ImageResize(&reSize, (int)gridInstance.gemShapes.width, (int)gridInstance.gemShapes.height);
				gemTextures[i] = LoadTextureFromImage(reSize);
				UnloadImage(reSize);

			}
		}
		void unLoadTexture()
		{
			for (int i = 0; i < 5; i++)
			{
				UnloadTexture(gemTextures[i]);
			}
			
		}
	}

}
