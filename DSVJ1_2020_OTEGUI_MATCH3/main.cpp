#include "raylib.h"
#include <iostream>
#include "gameplay.h"
using namespace std;
using namespace Match3;
int main()
{
	InitWindow(1920, 1080, "raylib [textures] example - background scrolling");
	gameplay::init();
	while (!WindowShouldClose())
	{
		gameplay::game();
	}
	gameplay::deInit();
	//gameplay::deInit();
	CloseWindow();
	return 0;
}