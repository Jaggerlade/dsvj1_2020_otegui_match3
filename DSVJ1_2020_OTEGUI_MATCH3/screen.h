#ifndef SCREEN_H
#define SCREEN_H
#include "raylib.h"
namespace Match3
{
	namespace screen
	{
		void init();
		void update();
		void draw();
		void deInit();
	}

}
#endif // !SCREEN_H

