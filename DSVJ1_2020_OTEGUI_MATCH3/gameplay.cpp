#include "gameplay.h"
#include "grid.h"
#include "player.h"
namespace Match3
{
	namespace gameplay
	{
		void init()
		{
			grid::init();
			
		}

		void update()
		{
			player::update();
			grid::update();
		}

		void draw()
		{
			ClearBackground(WHITE);
			BeginDrawing();
			grid::draw();
			EndDrawing();
		}

		void deInit()
		{
			grid::deInit();
		}

		void game()
		{
			update();
			draw();
		}

	}

}
