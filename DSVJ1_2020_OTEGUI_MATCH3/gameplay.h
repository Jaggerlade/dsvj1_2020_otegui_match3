#ifndef GAMEPLAY_H
#define GAMEPLAY_H	
namespace Match3 
{
	namespace gameplay 
	{
		void init();
		void update();
		void draw();
		void deInit();
		void game();
	}

}
#endif // !GAMEPLAY_H

