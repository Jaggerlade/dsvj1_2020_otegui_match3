#ifndef GRID_H
#define GRID_H
#include "raylib.h"
namespace Match3
{
	namespace grid
	{
		const int tamX = 10;
		const int tamY = 10;

		enum COLOR_TYPE
		{
			black,
			blue,
			green,
			purple,
			red,
			empty
		};
		struct GRID
		{
			COLOR_TYPE gemColor;
			Color colorGem;
			Rectangle gridShape;
			Rectangle gemShapes;
			
		};
		extern GRID gridArr[tamX][tamY];
		extern GRID gridInstance;
		void init();
		void update();
		void draw();
		void deInit();
		void loadTexture();
		void unLoadTexture();
	}
	

}
#endif // !GRID_H
